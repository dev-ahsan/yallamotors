# README

This is an assessment task for scraping cars model categories from Yalla Motors.

Things you may want to cover:

* Ruby version
```ruby 2.6.6```

* Rails version
```rails 5.2.3```

* Database
```postgresql```

* Gem used for scraping
```nokogiri```

* Other Gems
1. Form submission 
```simple_form```
2. Inline HTML templating system
```haml-rails```

* Output results shown via
```jquery datatable```

* Continuous Integration, Deployment & Delivery with 
```codeship```

Credits
-------

For information about the source please contact:

Ahsan Awan
devahsan.awan@gmail.com