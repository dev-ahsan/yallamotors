# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


$(document).ready ->
  $('#urls-table').dataTable()

  validateform = ->
    $('.error_messages').text('')
    input_url = $('#input_url_url').val()
    matcher   = new RegExp('^https?://(?:uae\\.)?yallamotor\\.com/new-cars?.*')

    if !(matcher.test(input_url))
      $('.error_messages').text('* Not a valid URL')
      false
    else
      splitted_url = input_url.replace('https://uae.yallamotor.com/new-cars/', '').split('/')
      if splitted_url.length > 1 && splitted_url[1] != ''
        $('.error_messages').text('* Not a valid URL')
        false
      else
        true

  window.validateform = validateform


