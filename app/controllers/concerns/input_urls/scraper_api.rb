module InputUrls::ScraperApi
  extend ActiveSupport::Concern

  require 'open-uri'

  def get_page target_url
    @doc   = Nokogiri::HTML(open(target_url.url))
    models = get_car_models(target_url)
    get_model_versions target_url
  end 

  def get_car_models target_url
    models = []
    @doc.css('.list-models').each do |type|
      if type.css('a.lazy_models').length > 0
        model = type.css('h3').first.text.strip
        url   =  type.css('a.lazy_models').first.attribute('href').value 
        target_url.car_models.create(name: model, url: "https://uae.yallamotor.com#{url}")
      end
    end
    models
  end

  def get_model_versions target_url
    target_url.car_models.each do |car_model|
      Nokogiri::HTML(open(car_model.url)).css('div.match-version').each do |version|
        if version.css('h3.pro-name').length > 0
          version_name = version.css('h3.pro-name').first.text.strip
        end
        if version.css('span.pro-pice').length > 0
          version_price = version.css('span.pro-pice').first.text.strip
        end
        car_model.model_versions.create(version_name: version_name, version_price: version_price)
      end
    end
  end
end