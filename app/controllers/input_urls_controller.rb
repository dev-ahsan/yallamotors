class InputUrlsController < ApplicationController

  include InputUrls::ScraperApi

  before_action :set_input_url, only: [:show, :destroy]

  def index
    @input_url  = InputUrl.new
    set_all_urls
  end

  def create
    input_url = InputUrl.new(url_params)
    if input_url.save
      get_page(input_url)
      flash!(notice: t('controllers.input_urls.create.flash.notice'))
    else
      flash!(alert: t('.alert', msg: input_url.errors.full_messages.to_sentence))
    end
    set_all_urls
    redirect_to root_path
  end

  def show
    @car_models = @input_url.car_models.includes(:model_versions)
  end

  def destroy
    @input_url.destroy
    set_all_urls
    redirect_to root_path
  end

  private

    def set_input_url
      @input_url = InputUrl.find_by_id(params[:id])
    end

    def url_params
      params.require(:input_url).permit(:url)
    end

    def set_all_urls
      @input_urls = InputUrl.all
    end
end
