class CarModel < ApplicationRecord

  belongs_to :input_url
  has_many :model_versions, dependent: :destroy
end
