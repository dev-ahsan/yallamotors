class InputUrl < ApplicationRecord

  has_many :car_models, dependent: :destroy

  validates_presence_of :url
  validates :url, format: URI::regexp(%w[http https])
  
end
