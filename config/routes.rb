Rails.application.routes.draw do
  get 'input_urls/index'

  root 'input_urls#index'


  resources :input_urls, except: [:edit, :update]
end
