class CreateInputUrls < ActiveRecord::Migration[5.2]
  def change
    create_table :input_urls do |t|
      t.string :url
      t.timestamps
    end
  end
end
