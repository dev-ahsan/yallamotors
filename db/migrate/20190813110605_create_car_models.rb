class CreateCarModels < ActiveRecord::Migration[5.2]
  def change
    create_table :car_models do |t|
      t.references :input_url, index: true, foreign_key: true
      t.string :name
      t.string :url
      t.timestamps
    end
  end
end
