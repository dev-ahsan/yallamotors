class CreateModelVersions < ActiveRecord::Migration[5.2]
  def change
    create_table :model_versions do |t|
      t.references :car_model, index: true, foreign_key: true
      t.string :version_name
      t.string :version_price
      t.timestamps
    end
  end
end
